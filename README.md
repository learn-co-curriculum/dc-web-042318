<iframe src="https://calendar.google.com/calendar/b/1/embed?showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;mode=WEEK&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=flatironschool.com_50tfj50t2i81q13cgoj6ri2tlo%40group.calendar.google.com&amp;color=%23B1365F&amp;ctz=America%2FNew_York" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

#### Module 1 Lecture Code and Videos

* 01 [Hashketball Review](https://youtu.be/MINUrRLCWtM) | [code](https://github.com/learn-co-students/dc-web-042318/tree/master/01-hasketball-review)  
* 02 [Hashes and APIs](https://youtu.be/F61WyHVPnfA) | [code](https://github.com/learn-co-students/dc-web-042318/tree/master/02-hashes-and-apis)
* 03 [Intro to OO](https://youtu.be/OJMkqWYjtFU) | [code](https://github.com/learn-co-students/dc-web-042318/tree/master/03-intro-to-oo)
* 03b [OO Review Code](https://github.com/learn-co-students/dc-web-042318/tree/master/05-oo-review)
* 04 [One to Many](https://www.youtube.com/watch?v=fugImeZxeKI) | [code](https://github.com/learn-co-students/dc-web-042318/tree/master/04-one-to-many)
* 05 [Many to Many](https://www.youtube.com/watch?v=OQHwDIFgir0) | [code](https://github.com/learn-co-students/dc-web-042318/tree/master/05-many-to-many)
* 06 [Intro to SQL](https://youtu.be/NMErFG7HZrs) | [code](https://github.com/learn-co-students/dc-web-042318/tree/master/06-intro-to-sql)  
* 07 [Intro to ORM](https://www.youtube.com/M75IxyRF2j0) | [code](https://github.com/learn-co-students/dc-web-042318/tree/master/07-intro-to-orms)
* 08 [Object Architecture](https://youtu.be/KW_cVEcmjMY) | [code](https://github.com/learn-co-students/dc-web-042318/tree/master/08-oo-architecture)  
* 10 [Intro to Active Record](https://www.youtube.com/watch?v=TOYFjiNsHZQ) | [code](https://github.com/learn-co-students/dc-web-042318/tree/master/10-intro-to-active-record)
* 11 [OO Review](https://youtu.be/XDj7BvNtT6Q) | [code](https://github.com/learn-co-students/dc-web-042318/tree/master/11-oo-review)
* 12 [Active Record Relationships](https://www.youtube.com/watch?v=Q1z-oCTIbdM) | [code](https://github.com/learn-co-students/dc-web-042318/tree/master/12-active-record-associations)

#### Module 2 Lecture Code and Videos

* 13 [CLI to Rack](https://www.youtube.com/watch?v=0EV5RQXmAYQ) | [code](https://github.com/learn-co-students/dc-web-042318/tree/master/13-cli-to-rack)
* 14 [Intro to Sinatra / MVC](https://youtu.be/piMXVmtRQX8)| [code](https://github.com/learn-co-students/dc-web-042318/tree/lecture)
* 15 [Sinatra Forms / Putting the 'CR' in 'CRUD'](https://youtu.be/hooNKxT10Ps ) | [code](https://github.com/learn-co-students/dc-web-042318/tree/master/15-sinatra-forms)
* 16 [More Sinatra Forms](https://youtu.be/S0RAq3XpafI) | [code](https://github.com/learn-co-students/dc-web-042318/tree/master/16-more-sinatra-forms)
* 17 [Intro To Rails](https://youtu.be/tJurcsItcTA) | [code](https://github.com/learn-co-students/dc-web-042318/tree/master/17-intro-to-rails)
* 18 [Rails CRUD / Forms](https://youtu.be/7dAUWWk2BMU) | [code](https://github.com/learn-co-students/dc-web-042318/tree/master/18-rails-forms)
* 19 [Rails Associations / Forms](https://youtu.be/egpBYIA5-UM) | [code](https://github.com/learn-co-students/dc-web-042318/tree/master/19-rails-associations)

#### Blog Presentation Schedule

May 10th - Richard, Adam, Josh, Kasia   
May 17th - Deep, Munir, Lucas, Brittany   
May 24th - Richard, Adam, Josh, Kasia   
May 31st - Deep, Munir, Lucas, Brittany  
June 7th - Richard, Adam, Josh, Kasia  
June 14th - Deep, Munir, Lucas, Brittany  
June 21st - Richard, Adam, Josh, Kasia  
June 28th - Deep, Munir, Lucas, Brittany  
July 5th - Richard, Adam, Josh, Kasia  
July 12th - Deep, Munir, Lucas, Brittany  
